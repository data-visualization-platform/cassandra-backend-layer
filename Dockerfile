FROM cassandra:latest

ARG CASSANDRA_TOKEN
ENV CASSANDRA_TOKEN=$CASSANDRA_TOKEN

USER root

# Place cluster-node startup-config script
ADD ./cassandra-clusternode.sh /usr/local/bin/cassandra-clusternode

# Copy the keyspace generator
COPY entrypoint-wrap.sh /entrypoint-wrap.sh

# Start Cassandra
ENTRYPOINT ["/entrypoint-wrap.sh", "cassandra-clusternode"]