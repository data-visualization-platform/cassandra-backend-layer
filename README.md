<h1> Data Visualization Platform </h1>
<hr>

# Setup

```sudo ./main.sh```

# Studio Interface 

https://downloads.datastax.com/#devcenter

### Connect
https://dbschema.com/documentation/Cassandra/

# Setup using Docker Registry

```
sudo docker pull registry.gitlab.com/data-visualization-platform/cassandra-backend-layer:latest
sudo docker run -p 9160:9160 -p 9042:9042 registry.gitlab.com/data-visualization-platform/cassandra-backend-layer:latest
```