#!/bin/bash

CASSANDRA_HOST_IP=`hostname --ip-address | cut -f 1 -d ' '`
echo $CASSANDRA_HOST_IP

# Create default keyspace for single node cluster
CQL="CREATE KEYSPACE IF NOT EXISTS trincom WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1};"
until echo $CQL | cqlsh $CASSANDRA_HOST_IP; do
echo "cqlsh: Cassandra is unavailable - retry later"
sleep 10
done &

exec /docker-entrypoint.sh "$@"